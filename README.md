# Backup van de website <http://braun-km.nl/>

De website <http://braun-km.nl/> bevatte handige informatie over de Braun KM keukenmachines, maar is helaas recent offline gegaan. Daarom heb ik hier een backup geplaatst. De pagina is nu te bezoeken op <https://fvdbeek.gitlab.io/braun-km.nl/>.
